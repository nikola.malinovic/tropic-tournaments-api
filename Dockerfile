# BUILD IMAGE:
# docker build -t tropik/tournaments-api .

# RUN CONTAINER:
# docker container run --rm -p 8080:8080 tropik/tournaments-api

FROM openjdk:11-jre-slim
ARG JAR_FILE=target/tournaments*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
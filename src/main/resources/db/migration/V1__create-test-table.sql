create table test
(
    id serial not null constraint person_pk primary key,
    message varchar
);

insert into test (id, message)
values (1, 'Flyway works');
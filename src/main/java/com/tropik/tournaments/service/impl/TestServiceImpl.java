package com.tropik.tournaments.service.impl;

import com.tropik.tournaments.entity.Test;
import com.tropik.tournaments.repository.TestRepository;
import com.tropik.tournaments.service.TestService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TestServiceImpl implements TestService {

	private final TestRepository testRepository;

	@Override
	public Optional<Test> findById(int id) {
		return testRepository.findById(id);
	}
}

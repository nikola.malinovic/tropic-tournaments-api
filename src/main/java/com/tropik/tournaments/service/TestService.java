package com.tropik.tournaments.service;

import com.tropik.tournaments.entity.Test;

import java.util.Optional;

public interface TestService {

	Optional<Test> findById(int id);
}

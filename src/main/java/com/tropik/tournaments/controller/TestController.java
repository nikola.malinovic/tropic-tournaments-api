package com.tropik.tournaments.controller;

import com.tropik.tournaments.entity.Test;
import com.tropik.tournaments.service.TestService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class TestController {

	private final TestService testService;

	@Value("${tropik.test}")
	private String test;

	@RequestMapping(value = "/")
	public Test testEndpoint() {
		Optional<Test> test = testService.findById(1);
		return test.orElse(null);
	}
}
